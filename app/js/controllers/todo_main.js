todo.controller('todo_main', ['$rootScope', "$scope", "addTodo", function($rootScope, $scope, addTodo) {

  $scope.submetForm = function(form) {
    var data = {
      "username": form.username.$viewValue,
      "todo": form.todo.$viewValue,
      "isDone": form.is_done.$viewValue,
      "hasAttachment": form.has_attachment.$viewValue
    };
    addTodo.add(data).then(function(res) {
      console.log("res");
    }, function(err) {
      console.log(err);
    })
  }


}]);