todo.config(['$stateProvider', '$urlRouterProvider', 'RestangularProvider', function($stateProvider, $urlRouterProvider, RestangularProvider) {

  $urlRouterProvider.otherwise("/");
  RestangularProvider.setBaseUrl('http://localhost:3000');

  RestangularProvider.setDefaultHttpFields({
    withCredentials: true
  });
  $stateProvider
    .state('home', {
      url: '/',
      controller: "todo_main",
      templateUrl: 'app/view/todo.html'
    });

  $urlRouterProvider.otherwise('/');

}]);