todo.service('addTodo', ["$q", "Restangular", function($q, Restangular) {

  var add = function(data) {
    console.log("data", data);
    return $q(function(resolve, reject) {
      Restangular.all('/api/todo').customPOST(data).then(function(res) {
        resolve(res);
        console.log("res", res);
      }, function(err) {
        reject(err);
        console.log("err", err);
      })
    });
  }
  return {
    "add": add
  };
}]);