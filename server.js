var express = require('express');
var morgan = require('morgan');
var methodOverride = require('method-override');
var request = require('request');
var bodyParser = require('body-parser');

var app = express();

var port = process.env.PORT || 7000

app.engine('jade', require('jade').__express);

app.use('/node-modules', express.static(__dirname + '/node_modules'));
app.use('/bower_components', express.static(__dirname + '/bower_components'));
app.use('/app', express.static(__dirname + '/app'));
app.use('/dist', express.static(__dirname + '/dist'));

app.use(morgan('dev'));

app.use(bodyParser.urlencoded({extended: true}));

app.use(bodyParser.json());
app.use(bodyParser.json({type: 'application/vnd.api+json'}));

app.use(methodOverride());

app.all('/*', function(req, res) {
  res.sendFile(__dirname + '/index.html');
});

console.log("app is listen on port", port);
console.log("Jenkins user test");
console.log("Jenkins user test2222");

app.listen(port);
